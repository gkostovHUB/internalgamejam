using TMPro;
using UnityEngine;

public class ResourceData : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_Label;
    [SerializeField] private ResourceType m_Type;

    public void SetAmount(string amount)
    {
        m_Label.text = amount;
    }

    public ResourceType GetResourceType()
    {
        return m_Type;
    }
}