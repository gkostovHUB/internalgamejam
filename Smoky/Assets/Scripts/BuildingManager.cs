using DG.Tweening;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class BuildingManager : MonoBehaviour
{
    public static BuildingManager Instance { get; private set; }

    [SerializeField] private Transform m_BuildingsParent = default;
    [Range(10.0f, 100.0f)][SerializeField] private float m_MaxConstructionRadius = default;
    private BuildingType m_ActiveBuildingType = default;

    public event EventHandler<OnActiveBuildingTypeChangedEventHandler> OnActiveBuildingChanged;

    [SerializeField] private BuildingGhost m_BuildingGhost;
    [SerializeField] private Transform m_ZeusIsland;

    [SerializeField] private GameObject m_BattleButton;
    [SerializeField] private Hades m_Hades;
    [SerializeField] private GameObject m_Fireball;
    [SerializeField] private GameObject m_BuildingEffect;
    [SerializeField] private Transform m_HellBin;
    [SerializeField] private Transform m_ConstructionBin;
    [SerializeField] private TextMeshProUGUI m_MonksText;

    [SerializeField] private GameObject[] m_Monks = new GameObject[3];

    [SerializeField] private GameObject m_FirstBuilding;

    [SerializeField] private ResourceAmount[] m_MonkCost;

    private List<GameObject> m_Buildings = new List<GameObject>();
    private int m_MonksAvailible = 0;
    private int m_MonksUICounter = 0;

    public bool CanSummonMonks { get; private set; }

    public Transform ZeusBuilding { get; private set; }

    public class OnActiveBuildingTypeChangedEventHandler : EventArgs
    {
        public BuildingType buildingType;
    }

    private void Awake()
    {
        Instance = this;
        m_ActiveBuildingType = null;
        m_Hades.m_HadesFavor += HadesFavor;
    }

    private void Start()
    {
        m_Buildings.Add(m_FirstBuilding);
    }

    public void CreateMonk()
    {
        if (ResourceManager.Instance.CanAfford(m_MonkCost))
        {
            m_Monks[m_MonksAvailible].SetActive(true);
            m_MonksAvailible = m_MonksAvailible >= 2 ? m_MonksAvailible : ++m_MonksAvailible;
            m_MonksUICounter = m_MonksUICounter >= 3 ? m_MonksUICounter : ++m_MonksUICounter;
            m_MonksText.text = "(" + m_MonksUICounter + "/3)";
            ResourceManager.Instance.SpendResources(m_MonkCost);
        }
        else
        {
            ToolTipUI.Instance.ShowToolTip("Not enough wine", new ToolTipUI.ToolTipTimer { timer = 2.0f });
        }
    }

    private void HadesFavor(object sender, bool e)
    {
        int buildingIndex = UnityEngine.Random.Range(0, m_Buildings.Count);

        Sequence sequence = DOTween.Sequence().SetAutoKill(true);

        for (int i = 0; i < UnityEngine.Random.Range(15, 30); i++)
        {
            sequence.AppendCallback(() => 
            {
                GameObject fx = Instantiate(m_Fireball, m_Buildings[buildingIndex].transform.position + new Vector3(UnityEngine.Random.Range(-2.0f, 2.0f), UnityEngine.Random.Range(-2.0f, 2.0f)), Quaternion.identity, m_HellBin);
                Destroy(fx, 2.0f);
            });
            sequence.AppendInterval(UnityEngine.Random.Range(0.1f, 0.2f));
        }
        
        BuildingTypeHolder bType = m_Buildings[buildingIndex].GetComponent<BuildingTypeHolder>();

        if (bType.type.buildingTypeID == BuildingTypeID.Farm)
        {
            bType.SetNewSprite(bType.type.destroyedSprite);
            bType.StopWheatProduction();
        }
        if (bType.type.buildingTypeID == BuildingTypeID.Wine)
        {
            bType.SetNewSprite(bType.type.destroyedSprite);
            bType.StopWineProduction();
        }
    }

    public void SetActiveBuildingType(BuildingType building)
    {
        m_ActiveBuildingType = building;
        OnActiveBuildingChanged?.Invoke(this, new OnActiveBuildingTypeChangedEventHandler { buildingType = m_ActiveBuildingType });
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            if (m_ActiveBuildingType != null)
            {
                if (CanSpawnBuilding(m_ActiveBuildingType, Utilities.GetMouseWorldPosition(), out string errorMessage))
                {
                    if (ResourceManager.Instance.CanAfford(m_ActiveBuildingType.constructionResourceCostArray))
                    {
                        ResourceManager.Instance.SpendResources(m_ActiveBuildingType.constructionResourceCostArray);
                        GameObject building = Instantiate(m_ActiveBuildingType.prefab, m_BuildingGhost.BuildHere, Quaternion.identity, m_BuildingsParent.transform).gameObject;
                        m_Buildings.Add(building);
                        if (m_ActiveBuildingType.buildingTypeID == BuildingTypeID.Zeus)
                        {
                            m_ZeusIsland.DOScale(1.0f, 2.0f);
                        }
                        if (m_ActiveBuildingType.buildingTypeID == BuildingTypeID.Arena)
                        {
                            m_BattleButton.SetActive(true);
                        }
                        if (m_ActiveBuildingType.buildingTypeID == BuildingTypeID.Ares)
                        {
                            CanSummonMonks = true;
                            ZeusBuilding = building.transform;
                        }

                        SetActiveBuildingType(null);
                        GameObject fx = Instantiate(m_BuildingEffect, building.transform.position, Quaternion.identity, m_ConstructionBin);
                        Destroy(fx, 2.0f);
                    }
                    else
                    {
                        ToolTipUI.Instance.ShowToolTip("Not enough " + m_ActiveBuildingType.GetConstructionResourceCostString(), new ToolTipUI.ToolTipTimer { timer = 2.0f});
                    }
                }
                else
                {
                    ToolTipUI.Instance.ShowToolTip(errorMessage, new ToolTipUI.ToolTipTimer { timer = 2.0f});
                }
            }
        }
    }

    private bool CanSpawnBuilding(BuildingType building, Vector2 position, out string errorMessage)
    {
        BoxCollider2D collider = building.prefab.GetComponent<BoxCollider2D>();

        Collider2D[] colliders = Physics2D.OverlapBoxAll(position + collider.offset, collider.size, 0);

        bool isClear = colliders.Length == 0;
        if (!isClear)
        {
            errorMessage = "Area is not clear!";
            return false;
        }

        colliders = Physics2D.OverlapCircleAll(position, building.minConstructionRadius);

        foreach (Collider2D collider2D in colliders)
        {
            BuildingTypeHolder holder = collider2D.GetComponent<BuildingTypeHolder>();
            if (holder != null)
            {
                if (holder.type == building)
                {
                    errorMessage = "Too close to another building of the same type!";
                    return false;
                }
            }
        }

        colliders = Physics2D.OverlapCircleAll(position, m_MaxConstructionRadius);

        foreach (Collider2D collider2D in colliders)
        {
            BuildingTypeHolder holder = collider2D.GetComponent<BuildingTypeHolder>();
            if (holder != null)
            {
                errorMessage = string.Empty;
                return true;
            }
        }

        errorMessage = "Too far from any other buildings!";
        return false;
    }
}