using UnityEngine;
using UniRx;

public class HeroTarget : MonoBehaviour
{
    [SerializeField] private Camera m_Camera;
    [SerializeField] private GameObject m_Marker;
    [SerializeField] private Agent m_Agent;
    [SerializeField] private SimplePathFinding2D pf;

    private void Start()
    {
        m_Agent.IsStationary.Subscribe(newValue =>
        {
            if (!newValue)
            {
                m_Marker.transform.position = pf.NavToWorld(m_Agent.Path.GetPathPoint(m_Agent.Path.GetPathPointList().Count - 1));
                m_Marker.SetActive(true);
            }
            else
            {
                m_Marker.SetActive(false);
            }
            
        })
        .AddTo(this);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            transform.position = (Vector2)m_Camera.ScreenToWorldPoint(Input.mousePosition);
        }
    }
}