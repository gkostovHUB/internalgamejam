using UnityEngine;
using TMPro;

public class ResourceNearbyOverlay : MonoBehaviour
{
    [SerializeField] private TextMeshPro m_Label;

    private ResourceGeneratorData m_Data;

    private void Update()
    {
        int nearbyResourceAmount = ResourceGenerator.GetNearbyResourceAmount(m_Data, transform.position);
        float percent = Mathf.RoundToInt((float)nearbyResourceAmount / m_Data.maxResourceAmount * 100.0f);
        m_Label.text = percent + " %";
    }

    public void SetState(bool state, ResourceGeneratorData data)
    {
        m_Data = data;

        if (state)
        {
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}