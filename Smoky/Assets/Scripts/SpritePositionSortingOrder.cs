using UnityEngine;

public class SpritePositionSortingOrder : MonoBehaviour
{
    [SerializeField] private SpriteRenderer m_SpriteRenderer;
    [Range(1.0f, 20.0f)] [SerializeField] private float m_precisionMultiplier;

    [SerializeField] private float m_PositionOffsetY;

    private void Start()
    {
        m_SpriteRenderer.sortingOrder = (int)(-(transform.position.y + m_PositionOffsetY) * m_precisionMultiplier);
    }
}