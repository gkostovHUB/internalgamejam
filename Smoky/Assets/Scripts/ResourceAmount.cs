using UnityEngine;
using System;

[Serializable]
public class ResourceAmount
{
    public ResourceType type;
    public int amount;
}