using UnityEngine;
using System;

public class HealthSystem : MonoBehaviour
{
    private int m_HealthAmountMax;
    private int m_HealthAmount;

    public event EventHandler OnDamaged;
    public event EventHandler OnDied;

    private void Awake()
    {
        m_HealthAmount = m_HealthAmountMax;
    }

    public void Damage(int damage)
    {
        m_HealthAmount -= damage;
        m_HealthAmount = Mathf.Clamp(m_HealthAmount, 0, m_HealthAmountMax);

        OnDamaged?.Invoke(this, EventArgs.Empty);

        if (IsDead())
        {
            OnDied?.Invoke(this, EventArgs.Empty);
        }
    }

    public bool IsDead()
    {
        return m_HealthAmount <= 0;
    }

    public int GetHealthAmount()
    {
        return m_HealthAmount;
    }

    public bool IsFullHealth()
    {
        return m_HealthAmount == m_HealthAmountMax;
    }

    public void SetHealthAmountMax(int maxHealth, bool updateHealth)
    {
        m_HealthAmountMax = maxHealth;

        if (updateHealth)
        {
            m_HealthAmount = maxHealth;
        }
    }

    public float GetHealthAmountNormalized()
    {
        return (float)m_HealthAmount / m_HealthAmountMax;
    }
}