using UnityEngine;

public class BuildingGhost : MonoBehaviour
{
    [SerializeField] private SpriteRenderer m_Sprite;
    //[SerializeField] private ResourceNearbyOverlay m_Nearby;
    [SerializeField] private SimplePathFinding2D pf;

    public Vector2 BuildHere { get; private set; }

    private void Start()
    {
        SetState(false, null);

        BuildingManager.Instance.OnActiveBuildingChanged += OnActiveBuildingChanged;
    }

    private void OnActiveBuildingChanged(object sender, BuildingManager.OnActiveBuildingTypeChangedEventHandler e)
    {
        if (e.buildingType == null)
        {
            SetState(false, null);
            //m_Nearby.SetState(false, null);
        }
        else
        {
            SetState(true, e.buildingType.sprite);
            //m_Nearby.SetState(true, e.buildingType.data);
        }
    }

    public void SetState(bool state, Sprite sprite)
    {
        if (state)
        {
            m_Sprite.sprite = sprite;
        }

        m_Sprite.gameObject.SetActive(state);
    }

    private void Update()
    {
        transform.position = (Vector2)pf.NavToWorld(pf.WorldToNav(Camera.main.ScreenToWorldPoint(Input.mousePosition)));
        BuildHere = transform.position;
    }
}