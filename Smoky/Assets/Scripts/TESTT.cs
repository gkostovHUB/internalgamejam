using UnityEngine;
using UnityEngine.UI;
using UniRx;
using UniRx.Triggers;

public class TESTT : MonoBehaviour
{
    [SerializeField] private Button m_Button;

    private void Start()
    {
        m_Button.OnMouseEnterAsObservable().Subscribe(_ => 
        {
            Debug.Log("WTF !");
        })
        .AddTo(this);
    }
}
