using UnityEngine;

public class ResourcesUI : MonoBehaviour
{
    [SerializeField] private ResourceData m_Wheat;
    [SerializeField] private ResourceData m_Wine;

    private void Start()
    {
        m_Wheat.SetAmount("1000");
        ResourceManager.Instance.AddResource(m_Wheat.GetResourceType(), 1000);
        ResourceManager.Instance.OnResourceAmountChanged += OnResourceAmountChanged;
    }

    private void OnResourceAmountChanged(object sender, System.EventArgs e)
    {
        UpdateResources();
    }

    public void UpdateResources()
    {
        m_Wheat.SetAmount(ResourceManager.Instance.GetResourceAmount(m_Wheat.GetResourceType()).ToString());
        m_Wine.SetAmount(ResourceManager.Instance.GetResourceAmount(m_Wine.GetResourceType()).ToString());
    }
}