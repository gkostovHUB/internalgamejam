using DG.Tweening;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class BuildingTypeUI : MonoBehaviour
{
    public static BuildingTypeUI Instance { get; private set; }

    [SerializeField] private Image m_BuildingImage;
    [SerializeField] private TextMeshProUGUI m_Label;
    [SerializeField] private GameObject m_Wheat;
    [SerializeField] private GameObject m_Wine;
    [SerializeField] private GameObject m_Monk;
    [SerializeField] private Button m_AddWheatButton;
    [SerializeField] private Button m_AddWineButton;
    [SerializeField] private Button m_AddMonkButton;

    private CompositeDisposable m_DisposeHere = new CompositeDisposable();

    public SpriteRenderer OldBuildingSR { get; private set; }

    private void Awake()
    {
        Instance = this;
    }

    public void Show(BuildingType type, Wheat wheat, Wine wine, SpriteRenderer _sr)
    {
        OldBuildingSR = _sr;
        _sr.color = Color.green;
        m_DisposeHere.Clear();
        m_BuildingImage.sprite = type.sprite;
        m_Label.text = type.buildingTypeID.ToString();

        m_Wheat.SetActive(type.buildingTypeID == BuildingTypeID.Farm);
        m_Wine.SetActive(type.buildingTypeID == BuildingTypeID.Wine);
        m_Monk.SetActive((type.buildingTypeID == BuildingTypeID.Main) && BuildingManager.Instance.CanSummonMonks);

        m_AddWheatButton.OnClickAsObservable().Subscribe(_ =>
        {
            wheat.AddWheatField();
        })
        .AddTo(m_DisposeHere);

        m_AddWineButton.OnClickAsObservable().Subscribe(_ =>
        {
            wine.AddWineField();
        })
        .AddTo(m_DisposeHere);

        m_AddMonkButton.OnClickAsObservable().Subscribe(_ =>
        {
            BuildingManager.Instance.CreateMonk();
        })
        .AddTo(m_DisposeHere);

        transform.DOScaleX(1.0f, 0.1f);
    }

    public void Hide()
    {
        OldBuildingSR.color = Color.white;
        m_DisposeHere.Clear();
        transform.DOScaleX(0.0f, 0.1f);
    }
}