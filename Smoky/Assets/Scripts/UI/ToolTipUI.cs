using TMPro;
using UnityEngine;
using DG.Tweening;

public class ToolTipUI : MonoBehaviour
{
    public static ToolTipUI Instance { get; private set; }

    private ToolTipTimer m_ToolTipTimer;

    private void Awake()
    {
        Instance = this;
        Hide();
    }

    private void Update()
    {
        if (m_ToolTipTimer != null)
        {
            m_ToolTipTimer.timer -= Time.deltaTime;
            if (m_ToolTipTimer.timer <= 0)
            {
                m_ToolTipTimer = null;
                Hide();
            }
        }
    }

    [SerializeField] private TextMeshProUGUI m_Label;

    private void SetText(string text)
    {
        m_Label.text = text;
    }

    public void ShowToolTip(string text, ToolTipTimer toolTipTimer = null)
    {
        m_ToolTipTimer = toolTipTimer;
        transform.DOScale(Vector3.one, 0.5f);
        SetText(text);
    }

    public void Hide()
    {
        transform.DOScale(Vector3.zero, 0.5f);
        SetText(string.Empty);
    }

    public class ToolTipTimer
    {
        public float timer;
    }
}