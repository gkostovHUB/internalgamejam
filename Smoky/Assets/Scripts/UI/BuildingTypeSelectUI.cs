using UnityEngine;
using UnityEngine.UI;
using UniRx;
using UnityEngine.EventSystems;

public class BuildingTypeSelectUI : MonoBehaviour
{
    [SerializeField] private Button m_CursorButton;
    [SerializeField] private Button m_WoodHarvesterButton;
    [SerializeField] private Button m_StoneHarvesterButton;
    [SerializeField] private Button m_GoldHarvesterButton;

    [SerializeField] private ButtonEvents m_CursorButtonEvents;
    [SerializeField] private ButtonEvents m_WoodHarvesterButtonEvents;
    [SerializeField] private ButtonEvents m_StoneHarvesterButtonEvents;
    [SerializeField] private ButtonEvents m_GoldHarvesterButtonEvents;

    [SerializeField] private BuildingType m_WoodHarvester;
    [SerializeField] private BuildingType m_StoneHarvester;
    [SerializeField] private BuildingType m_GoldHarvester;

    [SerializeField] private GameObject m_CursorOutline;
    [SerializeField] private GameObject m_WoodHarvesterOutline;
    [SerializeField] private GameObject m_StoneHarvesterOutline;
    [SerializeField] private GameObject m_GoldHarvesterOutline;

    private void Start()
    {
        m_CursorButtonEvents.OnMouseEnter += (object sender, System.EventArgs e) => { ToolTipUI.Instance.ShowToolTip("Select"); };
        m_CursorButtonEvents.OnMouseExit += (object sender, System.EventArgs e) => { ToolTipUI.Instance.Hide(); };

        m_WoodHarvesterButtonEvents.OnMouseEnter += (object sender, System.EventArgs e) => { ToolTipUI.Instance.ShowToolTip("Wood harvester\n" + m_WoodHarvester.GetConstructionResourceCostString()); };
        m_WoodHarvesterButtonEvents.OnMouseExit += (object sender, System.EventArgs e) => { ToolTipUI.Instance.Hide(); };

        m_StoneHarvesterButtonEvents.OnMouseEnter += (object sender, System.EventArgs e) => { ToolTipUI.Instance.ShowToolTip("Stone harvester\n" + m_StoneHarvester.GetConstructionResourceCostString()); };
        m_StoneHarvesterButtonEvents.OnMouseExit += (object sender, System.EventArgs e) => { ToolTipUI.Instance.Hide(); };

        m_GoldHarvesterButtonEvents.OnMouseEnter += (object sender, System.EventArgs e) => { ToolTipUI.Instance.ShowToolTip("Gold harvester\n" + m_GoldHarvester.GetConstructionResourceCostString()); };
        m_GoldHarvesterButtonEvents.OnMouseExit += (object sender, System.EventArgs e) => { ToolTipUI.Instance.Hide(); };

        m_CursorButton.OnClickAsObservable().Subscribe(_ =>
        {
            BuildingManager.Instance.SetActiveBuildingType(null);
            SetBuildingOutline(cursor: true);
        })
        .AddTo(this);

        m_WoodHarvesterButton.OnClickAsObservable().Subscribe(_ => 
        {
            BuildingManager.Instance.SetActiveBuildingType(m_WoodHarvester);
            SetBuildingOutline(wood: true);
        })
        .AddTo(this);

        m_StoneHarvesterButton.OnClickAsObservable().Subscribe(_ =>
        {
            BuildingManager.Instance.SetActiveBuildingType(m_StoneHarvester);
            SetBuildingOutline(stone: true);
        })
        .AddTo(this);

        m_GoldHarvesterButton.OnClickAsObservable().Subscribe(_ =>
        {
            BuildingManager.Instance.SetActiveBuildingType(m_GoldHarvester);
            SetBuildingOutline(gold: true);
        })
        .AddTo(this);
    }

    private void M_CursorButtonEvents_OnMouseEnter(object sender, System.EventArgs e)
    {
        throw new System.NotImplementedException();
    }

    private void SetBuildingOutline(bool cursor = false, bool wood = false, bool stone = false, bool gold = false)
    {
        m_CursorOutline.SetActive(cursor);
        m_WoodHarvesterOutline.SetActive(wood);
        m_StoneHarvesterOutline.SetActive(stone);
        m_GoldHarvesterOutline.SetActive(gold);
    }
}