using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/ResourceType")]
public class ResourceType : ScriptableObject
{
    public string id;
    public string color;
    public string nameShort;
}