using UnityEngine;
using Cinemachine;

public class CameraHandler : MonoBehaviour
{
    [Range(0.5f, 100.0f)][SerializeField] private float m_MoveSpeed = 0.0f;
    [Range(0.5f, 100.0f)][SerializeField] private float m_ZoomAmount = 0.0f;
    [Range(0.5f, 10.0f)] [SerializeField] private float m_ZoomSpeed = 0.0f;

    [Range(0.5f, 100.0f)][SerializeField] private float m_MinZoom = 0.0f;
    [Range(0.5f, 100.0f)][SerializeField] private float m_MaxZoom = 0.0f;

    [SerializeField] private CinemachineVirtualCamera m_Cinemachine;

    private float m_OrthographicSize;
    private float m_TargetOrthographicSize;

    private void Start()
    {
        m_OrthographicSize = m_Cinemachine.m_Lens.OrthographicSize;
        m_TargetOrthographicSize = m_OrthographicSize;
    }

    void LateUpdate()
    {
        HandleMovement();
        HandleZoom();
    }

    private void HandleMovement()
    {
        float x = Input.GetAxisRaw("Horizontal");
        float y = Input.GetAxisRaw("Vertical");

        Vector3 moveDir = new Vector2(x, y).normalized;

        transform.position += moveDir * m_MoveSpeed * Time.deltaTime;
    }

    private void HandleZoom()
    {
        m_TargetOrthographicSize += -Input.mouseScrollDelta.y * m_ZoomAmount;
        m_TargetOrthographicSize = Mathf.Clamp(m_TargetOrthographicSize, m_MinZoom, m_MaxZoom);

        m_OrthographicSize = Mathf.Lerp(m_OrthographicSize, m_TargetOrthographicSize, Time.deltaTime * m_ZoomSpeed);

        m_Cinemachine.m_Lens.OrthographicSize = m_OrthographicSize;
    }
}