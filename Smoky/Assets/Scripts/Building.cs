using UnityEngine;

public class Building : MonoBehaviour
{
    [SerializeField] private HealthSystem m_HealthSystem;
    [SerializeField] private BuildingTypeHolder m_BuildingTypeHolder;

    private void Start()
    {
        m_HealthSystem.OnDied += OnDied;
        m_HealthSystem.SetHealthAmountMax(m_BuildingTypeHolder.type.healthAmountMax, true);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            m_HealthSystem.Damage(10);
        }
    }

    private void OnDied(object sender, System.EventArgs e)
    {
        Destroy(gameObject);
    }
}