using UnityEngine;

public static class Utilities
{
    private static Camera m_MainCamera;

    public static Vector2 GetMouseWorldPosition()
    {
        if (m_MainCamera == null)
        {
            m_MainCamera = Camera.main;
        }

        return m_MainCamera.ScreenToWorldPoint(Input.mousePosition);
    }
}