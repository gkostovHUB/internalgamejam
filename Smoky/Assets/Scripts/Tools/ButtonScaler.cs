using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/*
 * Simple component, that when attached to a button will create a scale animation when the button is clicked.
 */
[RequireComponent(typeof(Button))]
public class ButtonScaler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public RectTransform targetTransform;

    public Vector3 normalScale = new Vector3(1f, 1f, 1f);
    public Vector3 clickedScale = new Vector3(0.95f, 0.95f, 1f);

    private Button button;

    private void Start()
    {
        if (targetTransform == null)
        {
            targetTransform = GetComponent<RectTransform>();
        }

        button = GetComponent<Button>();
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        if (button.interactable)
        {
            targetTransform.localScale = clickedScale;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        targetTransform.localScale = normalScale;
    }
}