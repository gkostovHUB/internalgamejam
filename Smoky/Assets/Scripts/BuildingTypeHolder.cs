using UnityEngine;

public class BuildingTypeHolder : MonoBehaviour
{
    public BuildingType type;
    [SerializeField] private Wheat m_Wheat;
    [SerializeField] private Wine m_Wine;
    [SerializeField] private SpriteRenderer m_Sr;

    private void OnMouseDown()
    {
        if (BuildingTypeUI.Instance.OldBuildingSR != null)
        {
            BuildingTypeUI.Instance.OldBuildingSR.color = Color.white;
        }

        BuildingTypeUI.Instance.Show(type, m_Wheat, m_Wine, m_Sr);
    }

    public void SetNewSprite(Sprite spr)
    {
        m_Sr.sprite = spr;
    }

    public void StopWheatProduction()
    {
        m_Wheat.StopAllWheatProduction();
    }

    public void StopWineProduction()
    {
        m_Wine.StopAllWineProduction();
    }
}