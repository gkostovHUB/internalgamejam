using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/BuildingType")]
public class BuildingType : ScriptableObject
{
    public BuildingTypeID buildingTypeID;
    public Transform prefab;
    public ResourceGeneratorData data;
    public Sprite sprite;
    public Sprite destroyedSprite;
    public float minConstructionRadius;
    public ResourceAmount[] constructionResourceCostArray;
    public int healthAmountMax;

    public string GetConstructionResourceCostString()
    {
        string result = string.Empty;

        foreach (ResourceAmount amount in constructionResourceCostArray)
        {
            result += "<color=#" + amount.type.color + ">" + amount.type.nameShort + ": " + amount.amount + "</color> ";
        }

        return result;
    }
}

public enum BuildingTypeID
{
    Farm = 0,
    Arena = 1,
    Ares = 2,
    Armory = 3,
    Barracks = 4,
    Main = 5,
    Wheat = 6,
    Zeus = 7,
    Wine = 8,
    Grapes = 9
}