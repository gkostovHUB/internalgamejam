using UnityEngine;

public class ResourceGenerator : MonoBehaviour
{
    private ResourceGeneratorData m_Data;

    [SerializeField] BuildingTypeHolder m_Holder;

    private float m_Time;
    private float m_TimerMax;

    private void Awake()
    {
        m_Data = m_Holder.type.data;
        m_TimerMax = m_Data.timerMax;
    }

    private void Start()
    {
        int index = GetNearbyResourceAmount(m_Data, transform.position);

        if (index == 0)
        {
            enabled = false;
        }
        else
        {
            m_TimerMax = (m_Data.timerMax / 2f) +
                m_Data.timerMax *
                (1 - (float)index / m_Data.maxResourceAmount);
        }
    }

    private void Update()
    {
        m_Time -= Time.deltaTime;

        if (m_Time <= 0)
        {
            m_Time += m_TimerMax;
            ResourceManager.Instance.AddResource(m_Data.resourceType, 1);
        }
    }

    public ResourceGeneratorData GetResourceGeneratorData()
    {
        return m_Data;
    }

    public float GetTimerNormalized()
    {
        return m_Time / m_TimerMax;
    }

    public float GetAmountGeneratedPerSecond()
    {
        return 1 / m_TimerMax;
    }

    public static int GetNearbyResourceAmount(ResourceGeneratorData data, Vector2 position)
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(position, data.resourceDetectionRadius);

        int index = 0;

        foreach (Collider2D collider in colliders)
        {
            ResourceNode node = collider.GetComponent<ResourceNode>();

            if (node != null)
            {
                if (node.resourceType == data.resourceType)
                {
                    index++;
                }
            }
        }

        index = Mathf.Clamp(index, 0, data.maxResourceAmount);

        return index;
    }
}