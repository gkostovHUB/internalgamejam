using UnityEngine;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private HealthSystem m_HealthSystem;
    [SerializeField] private Transform m_Bar;

    private void Start()
    {
        UpdateHealthBarVisible();
        m_HealthSystem.OnDamaged += OnDamaged;
    }

    private void OnDamaged(object sender, System.EventArgs e)
    {
        UpdateBar();
        UpdateHealthBarVisible();
    }

    private void UpdateBar()
    {
        m_Bar.transform.localScale = new Vector2(m_HealthSystem.GetHealthAmountNormalized(), 1.0f);
    }

    private void UpdateHealthBarVisible()
    {
        gameObject.SetActive(!(m_HealthSystem.IsFullHealth()));
    }
}