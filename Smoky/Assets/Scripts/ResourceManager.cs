using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager : MonoBehaviour
{
    private Dictionary<ResourceType, int> m_ResourceAmountDictionary = new Dictionary<ResourceType, int>();
    private ResourceTypeList m_ResourceTypeList;

    public event EventHandler OnResourceAmountChanged;

    public static ResourceManager Instance { get; private set; }

    private void Awake()
    {
        Instance = this;

        m_ResourceTypeList = Resources.Load<ResourceTypeList>(typeof(ResourceTypeList).Name);

        foreach (ResourceType resource in m_ResourceTypeList.list)
        {
            m_ResourceAmountDictionary[resource] = 0;
        }
    }

    public void AddResource(ResourceType resource, int amount)
    {
        m_ResourceAmountDictionary[resource] += amount;
        OnResourceAmountChanged?.Invoke(this, EventArgs.Empty);
    }

    private void Update()
    { 
        if (Input.GetKeyDown(KeyCode.Space))
        {
            TestLogResourceAmountDictionary();
        }
    }

    private void TestLogResourceAmountDictionary()
    {
        foreach (ResourceType resource in m_ResourceAmountDictionary.Keys)
        {
            Debug.Log(resource.id + ": " + m_ResourceAmountDictionary[resource]);
        }
    }

    public int GetResourceAmount(ResourceType resource)
    {
        return m_ResourceAmountDictionary[resource];
    }

    public bool CanAfford(ResourceAmount[] amountArray)
    {
        foreach (ResourceAmount amount in amountArray)
        {
            if (GetResourceAmount(amount.type) >= amount.amount)
            {
                // BUY IT !
            }
            else
            {
                return false;
            }
        }

        return true;
    }

    public void SpendResources(ResourceAmount[] amountArray)
    {
        foreach (ResourceAmount amount in amountArray)
        {
            m_ResourceAmountDictionary[amount.type] -= amount.amount;
        }
        OnResourceAmountChanged?.Invoke(this, EventArgs.Empty);
    }
}