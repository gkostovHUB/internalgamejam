using UnityEngine;

public class Wheat : MonoBehaviour
{
    [SerializeField] private GameObject[] m_WheatFields = new GameObject[5];

    public void AddWheatField()
    {
        for (int i = 0; i < m_WheatFields.Length; i++)
        {
            if (m_WheatFields[i].activeSelf)
            {
                continue;
            }
            else
            {
                m_WheatFields[i].SetActive(true);
                break;
            }
        }
    }

    public void StopAllWheatProduction()
    {
        for (int i = 0; i < m_WheatFields.Length; i++)
        {
            m_WheatFields[i].SetActive(false);
        }
    }
}