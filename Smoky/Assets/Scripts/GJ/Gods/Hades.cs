using System;
using TMPro;
using UnityEngine;

public class Hades : MonoBehaviour
{
    [Range(1.0f, 1000.0f)] [SerializeField] private float m_MaxFavor;
    [Range(1.0f, 10.0f)] [SerializeField] private float m_FavorDecreaseSpeed;
    [SerializeField] private TextMeshProUGUI m_PercentText;
    [SerializeField] private Transform m_FavorBar;

    private float m_CurrentFavor;

    public event EventHandler<bool> m_HadesFavor;

    private void Start()
    {
        m_CurrentFavor = m_MaxFavor;
    }

    private void Update()
    {
        if (m_CurrentFavor > 0)
        {
            m_CurrentFavor -= Time.deltaTime * m_FavorDecreaseSpeed;
            m_CurrentFavor = Mathf.Clamp(m_CurrentFavor, 0, m_MaxFavor);
            m_FavorBar.localScale = new Vector2(GetFavorNormalized(), 1.0f);

            if (m_CurrentFavor <= 0)
            {
                m_HadesFavor?.Invoke(this, true);
                m_CurrentFavor += m_MaxFavor;
            }
        }

        m_PercentText.text = Mathf.RoundToInt(m_CurrentFavor) + " %";
    }

    private float GetFavorNormalized()
    {
        return m_CurrentFavor / m_MaxFavor;
    }
}