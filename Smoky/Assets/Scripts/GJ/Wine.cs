using UnityEngine;

public class Wine : MonoBehaviour
{
    [SerializeField] private GameObject[] m_WineFields = new GameObject[4];

    public void AddWineField()
    {
        for (int i = 0; i < m_WineFields.Length; i++)
        {
            if (m_WineFields[i].activeSelf)
            {
                continue;
            }
            else
            {
                m_WineFields[i].SetActive(true);
                break;
            }
        }
    }

    public void StopAllWineProduction()
    {
        for (int i = 0; i < m_WineFields.Length; i++)
        {
            m_WineFields[i].SetActive(false);
        }
    }
}