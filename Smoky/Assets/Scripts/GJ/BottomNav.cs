using UnityEngine;
using UnityEngine.UI;
using UniRx;
using DG.Tweening;
using UnityEngine.Video;

public class BottomNav : MonoBehaviour
{
    [SerializeField] private Button m_BuildButton;
    [SerializeField] private Button m_InfoButton;
    [SerializeField] private Button m_BattleButton;
    [SerializeField] private Button m_HideNavigation;
    [SerializeField] private Button m_HideInfo;

    [SerializeField] private VideoPlayer m_VideoPlayer;
    [SerializeField] private GameObject m_VideoIsPlayedHere;

    [SerializeField] private VideoClip m_HerculesBattle;
    [SerializeField] private VideoClip m_HydraWin;
    [SerializeField] private VideoClip m_HydraLose;

    [SerializeField] private Transform m_BuildingsNavigation;
    [SerializeField] private Transform m_InfoNavigation;
    [SerializeField] private Transform m_NavShowPosition;
    [SerializeField] private Transform m_NavHidePosition;

    [SerializeField] private BuildingType m_ZeusBuilding;
    [SerializeField] private BuildingType m_Arena;
    [SerializeField] private BuildingType m_Barracks;
    [SerializeField] private BuildingType m_Ares;
    [SerializeField] private BuildingType m_Armory;
    [SerializeField] private BuildingType m_Wheat;
    [SerializeField] private BuildingType m_Wine;

    [Header("Buildings")]
    [SerializeField] private Button m_ZeusButton;
    [SerializeField] private Button m_ArenaButton;
    [SerializeField] private Button m_BarracksButton;
    [SerializeField] private Button m_AresButton;
    [SerializeField] private Button m_ArmoryButton;
    [SerializeField] private Button m_WheatButton;
    [SerializeField] private Button m_WineButton;

    [Header("Infos")]
    [SerializeField] private Button m_HeroInfoButton;
    [SerializeField] private Button m_CityHallInfoButton;
    [SerializeField] private Button m_ZeusInfoButton;

    [Header("Button events")]
    [SerializeField] private ButtonEvents m_BuildButtonEvent;
    [SerializeField] private ButtonEvents m_InfoButtonEvent;
    [SerializeField] private ButtonEvents m_HideNavigationEvent;
    [SerializeField] private ButtonEvents m_HideInfoEvent;
    [SerializeField] private ButtonEvents m_ZeusButtonEvent;
    [SerializeField] private ButtonEvents m_ArenaButtonEvent;
    [SerializeField] private ButtonEvents m_BarracksButtonEvent;
    [SerializeField] private ButtonEvents m_AresButtonEvent;
    [SerializeField] private ButtonEvents m_ArmoryButtonEvent;
    [SerializeField] private ButtonEvents m_WheatButtonEvent;
    [SerializeField] private ButtonEvents m_WineButtonEvent;
    [SerializeField] private ButtonEvents m_BattleButtonEvent;

    [Header("Info Button events")]
    [SerializeField] private ButtonEvents m_HeroInfoButtonEvent;
    [SerializeField] private ButtonEvents m_CityHallInfoButtonEvent;
    [SerializeField] private ButtonEvents m_ZeusInfoButtonEvent;

    [Header("Info Panels")]
    [SerializeField] private Image m_HeroInfo;
    [SerializeField] private Image m_CityHallInfo;
    [SerializeField] private Image m_ZeusInfo;

    [Header("Close Panels")]
    [SerializeField] private Button m_HeroCloseButton;
    [SerializeField] private Button m_CityHallCloseButton;
    [SerializeField] private Button m_ZeusCloseButton;

    [SerializeField] private ResourceType m_ResourceTypeWheat;
    [SerializeField] private Button m_BattleHydra;

    void EndReached(VideoPlayer vp)
    {
        m_VideoIsPlayedHere.SetActive(false);
        ResourceManager.Instance.AddResource(m_ResourceTypeWheat, 100);
    }

    private void Start()
    {
        m_VideoPlayer.loopPointReached += EndReached;

        m_InfoButtonEvent.OnMouseEnter += (object sender, System.EventArgs e) => { ToolTipUI.Instance.ShowToolTip("Upgrades", new ToolTipUI.ToolTipTimer { timer = 2.0f }); };

        m_InfoButton.OnClickAsObservable().Subscribe(_ => 
        {
            BuildingManager.Instance.SetActiveBuildingType(null);
            ShowInfobar();
            HideNavbar();
        })
        .AddTo(this);

        m_BuildButtonEvent.OnMouseEnter += (object sender, System.EventArgs e) => { ToolTipUI.Instance.ShowToolTip("Show availible buildings", new ToolTipUI.ToolTipTimer { timer = 2.0f }); };

        m_BuildButton.OnClickAsObservable().Subscribe(_ =>
        {
            BuildingManager.Instance.SetActiveBuildingType(null);
            ShowNavbar();
            HideInfobar();
        })
        .AddTo(this);

        m_BattleButtonEvent.OnMouseEnter += (object sender, System.EventArgs e) => { ToolTipUI.Instance.ShowToolTip("Battle", new ToolTipUI.ToolTipTimer { timer = 2.0f }); };

        m_BattleButton.OnClickAsObservable().Subscribe(_ =>
        {
            m_VideoIsPlayedHere.SetActive(true);
            m_VideoPlayer.clip = m_HerculesBattle;
            m_VideoPlayer.Play();
        })
        .AddTo(this);

        m_HideNavigationEvent.OnMouseEnter += (object sender, System.EventArgs e) => { ToolTipUI.Instance.ShowToolTip("Hide menu", new ToolTipUI.ToolTipTimer { timer = 2.0f }); };

        m_HideNavigation.OnClickAsObservable().Subscribe(_ =>
        {
            BuildingManager.Instance.SetActiveBuildingType(null);
            HideNavbar();
        })
        .AddTo(this);

        m_HideInfoEvent.OnMouseEnter += (object sender, System.EventArgs e) => { ToolTipUI.Instance.ShowToolTip("Hide Info", new ToolTipUI.ToolTipTimer { timer = 2.0f }); };

        m_HideInfo.OnClickAsObservable().Subscribe(_ =>
        {
            BuildingManager.Instance.SetActiveBuildingType(null);
            HideInfobar();
        })
        .AddTo(this);


        m_ZeusButtonEvent.OnMouseEnter += (object sender, System.EventArgs e) => { ToolTipUI.Instance.ShowToolTip("Zeus", new ToolTipUI.ToolTipTimer { timer = 2.0f }); };

        m_ZeusButton.OnClickAsObservable().Subscribe(_ =>
        {
            BuildingManager.Instance.SetActiveBuildingType(m_ZeusBuilding);
            HideNavbar();
        })
        .AddTo(this);


        m_ArenaButtonEvent.OnMouseEnter += (object sender, System.EventArgs e) => { ToolTipUI.Instance.ShowToolTip("Arena", new ToolTipUI.ToolTipTimer { timer = 2.0f }); };

        m_ArenaButton.OnClickAsObservable().Subscribe(_ =>
        {
            BuildingManager.Instance.SetActiveBuildingType(m_Arena);
            HideNavbar();
        })
        .AddTo(this);


        m_BarracksButtonEvent.OnMouseEnter += (object sender, System.EventArgs e) => { ToolTipUI.Instance.ShowToolTip("Barracks", new ToolTipUI.ToolTipTimer { timer = 2.0f }); };

        m_BarracksButton.OnClickAsObservable().Subscribe(_ =>
        {
            BuildingManager.Instance.SetActiveBuildingType(m_Barracks);
            HideNavbar();
        })
        .AddTo(this);


        m_AresButtonEvent.OnMouseEnter += (object sender, System.EventArgs e) => { ToolTipUI.Instance.ShowToolTip("Ares", new ToolTipUI.ToolTipTimer { timer = 2.0f }); };

        m_AresButton.OnClickAsObservable().Subscribe(_ =>
        {
            BuildingManager.Instance.SetActiveBuildingType(m_Ares);
            HideNavbar();
        })
        .AddTo(this);

        m_ArmoryButtonEvent.OnMouseEnter += (object sender, System.EventArgs e) => { ToolTipUI.Instance.ShowToolTip("Armory", new ToolTipUI.ToolTipTimer { timer = 2.0f }); };

        m_ArmoryButton.OnClickAsObservable().Subscribe(_ =>
        {
            BuildingManager.Instance.SetActiveBuildingType(m_Armory);
            HideNavbar();
        })
        .AddTo(this);

        m_WheatButtonEvent.OnMouseEnter += (object sender, System.EventArgs e) => { ToolTipUI.Instance.ShowToolTip("Whaet farm", new ToolTipUI.ToolTipTimer { timer = 2.0f }); };

        m_WheatButton.OnClickAsObservable().Subscribe(_ =>
        {
            BuildingManager.Instance.SetActiveBuildingType(m_Wheat);
            HideNavbar();
        })
        .AddTo(this);

        m_WineButtonEvent.OnMouseEnter += (object sender, System.EventArgs e) => { ToolTipUI.Instance.ShowToolTip("Wine farm", new ToolTipUI.ToolTipTimer { timer = 2.0f }); };

        m_WineButton.OnClickAsObservable().Subscribe(_ =>
        {
            BuildingManager.Instance.SetActiveBuildingType(m_Wine);
            HideNavbar();
        })
        .AddTo(this);

        m_HeroInfoButtonEvent.OnMouseEnter += (object sender, System.EventArgs e) => { ToolTipUI.Instance.ShowToolTip("Hero Info", new ToolTipUI.ToolTipTimer { timer = 2.0f }); };

        m_HeroInfoButton.OnClickAsObservable().Subscribe(_ =>
        {
            m_HeroInfo.DOFade(1.0f, 1.0f);
            HideInfobar();
            m_HeroCloseButton.gameObject.SetActive(true);
        })
        .AddTo(this);

        m_HeroCloseButton.OnClickAsObservable().Subscribe(_ =>
        {
            m_HeroInfo.DOFade(0.0f, 1.0f);
            m_HeroCloseButton.gameObject.SetActive(false);
        })
        .AddTo(this);

        m_CityHallInfoButtonEvent.OnMouseEnter += (object sender, System.EventArgs e) => { ToolTipUI.Instance.ShowToolTip("City hall Info", new ToolTipUI.ToolTipTimer { timer = 2.0f }); };

        m_CityHallInfoButton.OnClickAsObservable().Subscribe(_ =>
        {
            m_CityHallInfo.DOFade(1.0f, 1.0f);
            HideInfobar();
            m_CityHallCloseButton.gameObject.SetActive(true);
        })
        .AddTo(this);

        m_CityHallCloseButton.OnClickAsObservable().Subscribe(_ =>
        {
            m_CityHallInfo.DOFade(0.0f, 1.0f);
            m_CityHallCloseButton.gameObject.SetActive(false);
        })
        .AddTo(this);

        m_ZeusInfoButtonEvent.OnMouseEnter += (object sender, System.EventArgs e) => { ToolTipUI.Instance.ShowToolTip("zeus Info", new ToolTipUI.ToolTipTimer { timer = 2.0f }); };

        m_ZeusInfoButton.OnClickAsObservable().Subscribe(_ =>
        {
            m_ZeusInfo.DOFade(1.0f, 1.0f);
            HideInfobar();
            m_ZeusCloseButton.gameObject.SetActive(true);
        })
        .AddTo(this);

        m_ZeusCloseButton.OnClickAsObservable().Subscribe(_ =>
        {
            m_ZeusInfo.DOFade(0.0f, 1.0f);
            m_ZeusCloseButton.gameObject.SetActive(false);
        })
        .AddTo(this);

        m_BattleHydra.OnClickAsObservable().Subscribe(_ =>
        {
            m_VideoIsPlayedHere.SetActive(true);
            m_VideoPlayer.clip = Random.Range(0, 2) > 0 ? m_HydraWin : m_HydraLose;
            m_VideoPlayer.Play();
        })
        .AddTo(this);
    }

    private void ShowNavbar()
    {
        m_BuildingsNavigation.DOMove(m_NavShowPosition.position, 0.2f);
        m_BuildingsNavigation.DOScale(Vector3.one, 0.5f);
    }

    private void HideNavbar()
    {
        m_BuildingsNavigation.DOMove(m_NavHidePosition.position, 0.2f);
        m_BuildingsNavigation.DOScale(Vector3.zero, 0.5f);
    }

    private void ShowInfobar()
    {
        m_InfoNavigation.DOMove(Vector2.zero, 0.2f);
        m_InfoNavigation.DOScale(Vector3.one, 0.5f);
    }

    private void HideInfobar()
    {
        m_InfoNavigation.DOMove(new Vector2(50.0f, -50.0f), 0.2f);
        m_InfoNavigation.DOScale(Vector3.zero, 0.5f);
    }
}