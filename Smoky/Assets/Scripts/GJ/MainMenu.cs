using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UniRx;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private Button m_PlayButton;
    [SerializeField] private Button m_CreditsButton;
    [SerializeField] private Button m_QuitButton;
    [SerializeField] private Button m_CloseCreditsButton;

    [SerializeField] private CanvasGroup m_CreditsPanel;

    private void Start()
    {
        m_PlayButton.OnClickAsObservable().Subscribe(_ => 
        {
            SceneManager.LoadScene("GJFianl");
        })
        .AddTo(this);

        m_QuitButton.OnClickAsObservable().Subscribe(_ =>
        {
            Application.Quit();
        })
        .AddTo(this);

        m_CreditsButton.OnClickAsObservable().Subscribe(_ =>
        {
            m_CreditsPanel.transform.DOScale(1.0f, 1.0f);
            m_CreditsPanel.DOFade(1.0f, 1.5f);
        })
        .AddTo(this);

        m_CloseCreditsButton.OnClickAsObservable().Subscribe(_ =>
        {
            m_CreditsPanel.transform.DOScale(0.0f, 1.5f);
            m_CreditsPanel.DOFade(0.0f, 1.0f);
        })
        .AddTo(this);
    }
}