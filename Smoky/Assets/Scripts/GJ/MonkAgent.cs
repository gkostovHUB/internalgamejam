using UniRx;
using UnityEngine;

public class MonkAgent : MonoBehaviour
{
    public SimplePF2D.Path Path { get; private set; }
    private Vector3 nextPoint;
    [HideInInspector] public ReactiveProperty<bool> IsStationary { get; private set; } = new ReactiveProperty<bool>(true);

    [Range(1.0f, 20.0f)] [SerializeField] private float m_MovementSpeed;
    [SerializeField] private SimplePathFinding2D pf;
    [SerializeField] private Rigidbody2D rb;


    private void Start()
    {
        Path = new SimplePF2D.Path(pf);
        nextPoint = Vector2.zero;

        Path.CreatePath(transform.position, BuildingManager.Instance.ZeusBuilding.transform.position);
    }

    private void Update()
    {
        CheckForMovement();
    }

    public void CheckForMovement()
    {
        if (Path.IsGenerated())
        {
            if (IsStationary.Value)
            {
                if (Path.GetNextPoint(ref nextPoint))
                {
                    rb.velocity = nextPoint - transform.position;
                    rb.velocity = rb.velocity.normalized;
                    rb.velocity *= m_MovementSpeed;
                    IsStationary.Value = false;
                }
                else
                {
                    rb.velocity = Vector3.zero;
                    IsStationary.Value = true;
                }
            }
            else
            {
                Vector3 delta = nextPoint - transform.position;
                if (delta.magnitude <= 0.2f)
                {
                    rb.velocity = Vector3.zero;
                    IsStationary.Value = true;
                }
            }
        }
        else
        {
            rb.velocity = Vector3.zero;
            IsStationary.Value = true;
        }
    }
}