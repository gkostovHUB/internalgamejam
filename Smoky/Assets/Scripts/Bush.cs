using System.Collections.Generic;
using UnityEngine;

public class Bush : MonoBehaviour
{
    [SerializeField] private List<Sprite> m_BushSprites;
    [SerializeField] private SpriteRenderer m_SpriteRenderer;

    private void Start()
    {
        m_SpriteRenderer.sprite = m_BushSprites[Random.Range(0, m_BushSprites.Count)];
    }
}