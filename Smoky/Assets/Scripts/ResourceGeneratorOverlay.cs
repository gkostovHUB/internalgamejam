using TMPro;
using UnityEngine;

public class ResourceGeneratorOverlay : MonoBehaviour
{
    [SerializeField] private ResourceGenerator m_ResourceGenerator;
    [SerializeField] private Transform m_Bar;
    [SerializeField] private TextMeshPro m_Label;

    private ResourceGeneratorData m_Data;

    private void Start()
    {
        m_Data = m_ResourceGenerator.GetResourceGeneratorData();
        m_Label.text = m_ResourceGenerator.GetAmountGeneratedPerSecond().ToString("F1");
    }

    private void Update()
    {
        m_Bar.localScale = new Vector2(1 - m_ResourceGenerator.GetTimerNormalized(), 1.0f);
    }
}