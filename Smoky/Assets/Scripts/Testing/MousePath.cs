using UnityEngine;

public class MousePath : MonoBehaviour
{
    [SerializeField] private SimplePathFinding2D pf;
    private SimplePF2D.Path path;

    private void Start()
    {
        path = new SimplePF2D.Path(pf);
    }

    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            path.CreatePath(Vector2.zero, mousePosition);
        }
    }
}